//
//  ViewController.swift
//  test
//
//  Created by urvashi khatuja on 8/3/17.
//  Copyright © 2017 urvashi khatuja. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var recordingLabel: UILabel!
    
    @IBOutlet weak var recordButton: UIButton!
    
    @IBOutlet weak var stopRecordingButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        var label = UILabel()
        label.frame = CGRect(x:150, y:150, width:60, height:60)
        label.text = "0"
        self.view.addSubview(label)
        stopRecordingButton.isEnabled = false
        
        var button = UIButton()
        button.frame = CGRect(x:150, y:150, width:60, height: 70)
        button.setTitle("click", for: .normal)
        button.setTitleColor(.red, for: .normal)
        self.view.addSubview(button)
        
        
        
        // Do any additional setup after loading the view, typically from a nib.
    }


    @IBAction func record_audio(_ sender: Any) {
        
        recordingLabel.text = "recording in progress"
        stopRecordingButton.isEnabled = true
        recordButton.isEnabled = false
    }
    @IBAction func stopRecording(_ sender: Any) {
        
        recordButton.isEnabled = true
        stopRecordingButton.isEnabled = false
        recordingLabel.text = "tap to record"
        
    }

}

